#!/usr/bin/env python

import argparse
import configparser

parser = argparse.ArgumentParser(description='Start Bartomaton Server or modify database.')
parser.add_argument('--database', '-db', default=None, choices=['create', 'migrate', 'upgrade', 'downgrade', 'createDrinkDB'], help='Use with options to create, migrate, downgrade, or upgrade the app database.')
parser.add_argument('--access', default='lan', choices=['lan','local'],help='Flask access over lan or localhost.')
parser.add_argument("--mock", default=False, action='store_true', help='Set this when running on host device to mock Pi-dependent features (GPIO)')
args = parser.parse_args()

config = configparser.ConfigParser()
config['DEFAULT'] = {}

if(args.database == None):
  print('Running Server...')

  if(args.mock):
    config['DEFAULT']['is_device'] = 'false'
  else:
    config['DEFAULT']['is_device'] = 'true'

  with open('config.ini', 'w') as configfile:
    config.write(configfile)

  #import flask object from app package
  from app import app, config

  #start server
  if args.access == 'lan':
    # access from local network
    app.run(host='0.0.0.0',debug=True)
  else:
    #access from local machine running server
    app.run(debug=True)

else:
  #database action
  from app.database import dbControl

  #print(app.config.SQLALCHEMY_DATABASE_URI)

  if(args.database == 'create'):
    print('Creating database...')
    dbControl.createDatabase()

  if(args.database == 'migrate'):
    print('Migrating database...')
    dbControl.migrateDatabase()

  if(args.database == 'upgrade'):
    print('Upgrading database...')
    dbControl.upgradeDatabase()

  if(args.database == 'createDrinkDB'):
    print('Creating database from CSV ...')
    dbControl.makeDrinkDbFromCSV()
