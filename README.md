# Bartomaton
Robotic Bartender using Raspberry Pi 3


# Build Info

Create a python environment

`python3 -m venv bartomaton_env`

install the softrequirements on all systems:

`pip install -r softrequirements.txt`

on Pi install the pi-specific python packages

`pip install -r requirements-pi.txt`

Install the pigpio daemon

`sudo apt-get install pigpio python-pigpio python3-pigpio`

Enable pigpio to run on boot

`sudo systemctl enable pigpiod`
`sudo systemctl start pigpiod`

## React Build

Install npm and nodejs, since Raspberry Pi is arm it will require an unofficial build install:

https://hassancorrigan.com/blog/install-nodejs-on-a-raspberry-pi-zero/

Intall packages from `package.json` by entering the app directory and using `npm install`

From the app folder run:
`./node_modules/webpack/bin/webpack.js -p --config ./webpack.config.js --display-error-details`
to build the jsx.

It's recommended to build on the host system and copy the resulting jsx to the device.