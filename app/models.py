from app import app, db
import json

### TO DO:
# Add comments on structure, relationship table

class Drink(db.Model):
    __tablename__ = 'drink'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), index=True, unique=True)
    recipe = db.relationship("DrinkIngredient", backref="drink")

    def __init__(self, name):
        self.id = id
        self.name = name
# 
    def __repr__(self):
        return '<Drink %r>' % (self.name.title())

    def to_json(self):
        json_drink = json.dumps({
            'id': self.id,
            "name": self.name,
            "recipe": [i.to_dict() for i in self.recipe]
        })
        return json_drink

    def to_dict(self):
        dict_drink = {
            'id': self.id,
            "name": self.name,
            "recipe": [i.to_dict() for i in self.recipe]
        }
        return dict_drink

class DrinkIngredient(db.Model):
    __tablename__ = 'drink_ingredient'
    drink_id = db.Column(db.Integer, db.ForeignKey("drink.id"), primary_key=True)
    ingredient_id = db.Column(db.Integer, db.ForeignKey("ingredient.id"), primary_key=True)
    units = db.Column(db.Numeric(4, 2)) # Total of 4 digits, 2 after decimal: 00.00

    ingredient = db.relationship("Ingredient")

    def __init__(self, ingredient=None, units=None):
        self.ingredient = ingredient
        self.units = units

    def __repr__(self):
        return '<DrinkIngredient %g units of %r>' % (self.units, self.ingredient)

    def to_json(self):
        json_drinkIngredient = json.dumps({
            "drink_id": self.drink_id,
            "ingredient_id" : self.ingredient_id,
            "ingredient": self.ingredient.name,
            "amount": self.units
        })
        return json_drinkIngredient

    def to_dict(self):
        dict_drinkIngredient = {
            "drink_id": self.drink_id,
            "ingredient_id" : self.ingredient_id,
            "ingredient": self.ingredient.name,
            "amount": float(self.units)
        }
        return dict_drinkIngredient

class Ingredient(db.Model):
    __tablename__ = 'ingredient'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(140))

    # subquery is also a valid option, if you want them loaded at query time. Select is the standard lazy loading
    # This needs to be viewonly, so new DrinkIngredients are spuriously created without units
    drinks = db.relationship("Drink", secondary=DrinkIngredient.__table__, lazy="select", viewonly=True)

    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return '<Ingredient %r>' % (self.name.title())

    def to_json(self):
        json_ingredient = json.dumps({
            "id" : self.id,
            "name": self.name
        })
        return json_ingredient

    def to_dict(self):
        dict_ingredient = {
            "id" : self.id,
            "name": self.name
        }
        return dict_ingredient

    @classmethod
    def get_or_create(cls, name):
        instance = cls.query.filter_by(name=name).first()
        if not instance:
            instance = cls(name)
            db.session.add(instance)
            db.session.flush([instance])
        return instance

class Dispenser(db.Model):
    __tablename__ = 'dispenser'
    id = db.Column(db.Integer, primary_key=True)
    servoID = db.Column(db.Integer, unique=True)
    pumpID = db.Column(db.Integer, unique=True)

    ingredient_id = db.Column(db.Integer, db.ForeignKey("ingredient.id"), unique=True)
    ingredient = db.relationship("Ingredient", backref=db.backref("dispenser", uselist=False))

    def __init__(self, servoID, pumpID, ingredient=None):
        self.servoID = servoID
        self.pumpID = pumpID
        self.ingredient = ingredient
        if self.ingredient is not None:
            self.ingredient_id = self.ingredient.id

    def __repr__(self):
        return '<Dispenser on Pump pin %s and Servo pin %s has %r>' % (str(self.pumpID), str(self.servoID), self.ingredient)

    def get_ingredient_name(self):
        if self.ingredient is not None:
            return self.ingredient.name
        else:
            return "None"

    def get_ingredient_id(self):
        if self.ingredient is not None:
            return self.ingredient.id
        else:
            return "None"

    def to_json(self):
        json_pump = json.dumps({
            "id": self.id,
            "servoID": self.servoID,
            "pumpID": self.pumpID,
            "ingredient_id" : self.get_ingredient_id(),
            "ingredient": self.get_ingredient_name()
        })
        return json_pump

    def to_dict(self):
        dict_pump = {
            "id": self.id,
            "servoID": self.servoID,
            "pumpID": self.pumpID,
            "ingredient_id" : self.get_ingredient_id(),
            "ingredientName": self.get_ingredient_name()
        }
        return dict_pump
