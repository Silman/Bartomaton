const path = require('path');

module.exports = {
    //entry is files that webpack will compile together
    entry: [path.join(__dirname, 'static', 'js', 'src', 'entry.js'),
            path.join(__dirname, 'static', 'js', 'src', 'entry.jsx')],
    
    mode: 'development',
    
    //this tells webpack to use babel to compile the JSX to Ecmascript 5 before packing it with the rest of the javascript
    module: {
        rules: [
        {
            loader:   'babel-loader',
            test:     /\.jsx$/,
            //use the preset-env and preset-react to compile jsx
            query: {
              presets: ['@babel/preset-env', '@babel/preset-react']
            }
        },
        {
              test: /\.css$/,  
              include: /node_modules/,  
              loaders: ['style-loader', 'css-loader'],
         }
        ]
    },

    //output defines the directory and file that webpack outputs
    output: {
        path:      path.join(__dirname, 'static', 'js', 'build'),
        filename:  'minimalistic_react.js'
      }
  };