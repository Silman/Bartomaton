import os
import configparser

# Define the application directory
basedir = os.path.abspath(os.path.dirname(__file__))

#manual static directory
staticdir = os.path.join(basedir, 'static')

# Disable caching
SEND_FILE_MAX_AGE_DEFAULT = 0

# Define the database - we are working with
# SQLite
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'database', 'app.db')
SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'database', 'db_repository')
SQLALCHEMY_TRACK_MODIFICATIONS = False

# Secret key for signing cookies
SECRET_KEY = 'you-will-never-guess'

# Enable protection agains *Cross-site Request Forgery (CSRF)*
CSRF_ENABLED = True

# Define whether this is running on host or device
# IS_DEVICE = False

# Load external configuration file params
external_config = configparser.ConfigParser()
external_config.read('config.ini')
print("External config is_device", external_config['DEFAULT'].getboolean('is_device'))

IS_DEVICE = external_config['DEFAULT'].getboolean('is_device')


##Pump Selector Pins - BCM GPIO on Raspberry Pi based on schematic
pumpSelect0 = 27
pumpSelect1 = 17
pumpSelect2 = 11
pumpSelect3 = 9
pumpInhibit = 10
pumpStrobe = 22

#Pump Control Pins
decoderControlGPIOPin = [pumpSelect0, pumpSelect1, pumpSelect2, pumpSelect3]
decoderControlInhibitPin = pumpInhibit
decoderControlStrobePin = pumpStrobe

#Servo Positions
#TODO: Find actual values that make servos open/close

SERVO_DC_OPEN = 9
SERVO_DC_CLOSE = 14
SERVO_DC_REV = 1

SERVO_PW_OPEN = 2750
SERVO_PW_CLOSE = 1750
SERVO_PW_REV = 300

#Load Cell Pins
loadcell_DOUT = 19
loadcell_SCK = 26