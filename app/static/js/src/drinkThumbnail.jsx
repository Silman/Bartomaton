import React from 'react';
import ReactDOM from 'react-dom';

import { useState } from 'react';
import {Image, Button } from 'react-bootstrap';

import DrinkInfo from './drinkInfo.jsx'

var config = require('./config');

const Results = () => (
  <div id="results" className="search-results">
    Some Results
  </div>
)

const InfoButton = (props) => {
  const [showInfo, setShowInfo] = React.useState(false);
  const onClick = () => setShowInfo(true);
  let closeInfo = () => this.setState({ showInfo: false });

  return (
    <div>
      <Button bsStyle="default" onClick={onClick}> Info </Button>
    
      { <DrinkInfo show={showInfo} onHide={setShowInfo(false)} drink={props.drink} /> }
    </div>
  )
}

export default class DrinkThumbnail extends React.Component {

  constructor(props){
    super(props);

    this.state = {
      showInfo: false,
    };

    //console.log('Drink Thumbnail for')
    //console.log(this.props.drink.name)
  }

  makeDrink(drink_id){
    var makeDrinkbyID = config.baseURL+'/makeDrinkID';

    var request = {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      method: "POST",
      body: JSON.stringify({drink_id: drink_id})
    };

    fetch(makeDrinkbyID, request)
      .then( (response) => {
        if(response.status >= 400){
          throw new Error('Bad response from server');
        }
        console.log('Response status from Drink Making endpoint: '+ response.status);
        return response.json();
      })
      .then( (data) => {
        alert('Making Drink!');
      });
  }    

  render(){
    let closeInfo = () => this.setState({ showInfo: false });

    return(
      <div>
        <Image src={this.props.image} fluid rounded/>
        <h3>{this.props.drink.name}</h3>
        {/* <p>{this.props.desc}</p> */}
        <p>
          <Button bsStyle="primary" onClick={()=>this.makeDrink(this.props.drink.id)}> Make! </Button> &nbsp;
          <Button bsStyle="default" onClick={()=>this.setState({ showInfo: true })}> Info </Button>          
        </p>
        <DrinkInfo show={this.state.showInfo} onHide={closeInfo} drink={this.props.drink} />

      </div>
    );
  }

}

