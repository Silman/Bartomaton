import React from 'react';
import ReactDOM from 'react-dom';

import {Container, Row, Col} from 'react-bootstrap';

import DrinkThumbnail from './drinkThumbnail.jsx'

export default class DrinkGrid extends React.Component {

  constructor(props){
    super(props);

    //console.log(this.props)
  }

  render(){

    //{console.log("Grid being created")}

    return(
      <div>
        <Container>
          <Row sm={1} md={4}>

              {this.props.drinks.map( (drink, value) =>
                <Col key={value}>
                
                <DrinkThumbnail key={value} drink={drink} desc={value} image='static/img/bartomaton_512_512.png' />

                </Col>
              )}
          </Row>
        </Container>
      </div>
    );
  }

}
