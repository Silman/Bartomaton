//JSX entry

console.log("JSX entry logic.");

require('es6-promise').polyfill();
require('isomorphic-fetch');

import React from 'react';
import ReactDOM from 'react-dom';

import {Grid, Row, Col, Modal, ButtonGroup, Button, Table} from 'react-bootstrap';
import Select from 'react-select';

var config = require('./config');
console.log("Base URL: " + config.baseURL);

// Be sure to include styles at some point, probably during your bootstrapping
//import 'react-select/dist/react-select.css';


import DrinkGrid from './drinkGrid.jsx'
import Dispensers from './dispensers.jsx'

class App extends React.Component {

  constructor(props){
    super(props);

    this.handleShow = this.handleShow.bind(this);
    this.handleClose = this.handleClose.bind(this);

    this.state = {
        drinkData: [],
        showDispensers: false
    }
    return;
   }

  componentDidMount(){
    var drinkDatabaseURL = config.baseURL+'/drink';

    fetch(drinkDatabaseURL)
      .then( (response) => {
        if(response.status >= 400){
          throw new Error('Bad response from server');
        }
        console.log('Response status from Drink Database API: '+ response.status);
        return response.json();
      })
      .then( (data) => {
        this.setState({drinkData: data});
      });
  }

  handleShow() {
    this.setState({
      showDispensers: true
    });
  }

  handleClose(){
    this.setState({
      showDispensers: false
    })
  }

  render(){
    if ( this.state.drinkData.length > 0 ) { //data has loaded
      {console.log("Drink data loaded")}

      //console.log(this.state.drinkData);

      return(
        <div>
          {/* <div> {JSON.stringify(this.state.drinkData)} </div> -- uncomment to get full drink JSON*/} 

          <Button bsStyle="default" onClick={this.handleShow}> Dispensers </Button>

          <Dispensers show={this.state.showDispensers} onHide={this.handleClose} dispensers={this.props.drink} />

          <DrinkGrid drinks={this.state.drinkData} />

        </div>
      );
    } else{
      {console.log("Waiting for Drink Data...")}
      return <h3> Loading Drinks.... </h3>;
    }
  }
}


ReactDOM.render(<App />, document.getElementById('react-app'));