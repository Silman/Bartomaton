import React from 'react';
import ReactDOM from 'react-dom';

import {Modal, ButtonGroup, Button, Table} from 'react-bootstrap';

var config = require('./config');


export default class DrinkInfo extends React.Component {

  constructor(props){
    super(props)

    var newRecipe = []

    props.drink.recipe.map( (di, value) => {
      newRecipe.push({ingredient_id: di.ingredient_id, ingredient: di.ingredient, amount: di.amount})
      return
    });

    // console.log('drink name')
    // console.log(props.drink.name)
    // console.log('newRecipe is')
    // console.log(newRecipe);

    this.state = {
      recipe: newRecipe,
    };

  }

  adjustAmount(ingredient, level){
    
    var tempRecipe = this.state.recipe.slice();

    tempRecipe[ingredient].amount = level * this.props.drink.recipe[ingredient].amount;
    //tempRecipe[ingredient].amount = level * this.props.drink.recipe[ingredient].amount;

    this.setState({ recipe: tempRecipe});
  }

  makeDrinkCustom(recipe){
    var makeDrinkbyRecipe = config.baseURL+'/makeDrinkCustom';

    var request = {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      method: "POST",
      body: JSON.stringify({recipe: recipe})
    };

    fetch(makeDrinkbyRecipe, request)
      .then( (response) => {
        if(response.status >= 400){
          throw new Error('Bad response from server');
        }
        console.log('Response status from Recipe Making endpoint: '+ response.status);
        return response.json();
      })
      .then( (data) => {
        alert('Making Drink!');
      });
  }

  render(){

    const drinkName = this.props.drink.drink;
    //console.log(this.state.recipe)

    const recipeTableBody = this.state.recipe.map((di, value) => {
      //console.log(di.ingredient);
      return( 
        <tr key = {di.ingredient}>
          <td>{di.ingredient}</td>
          <td>{di.amount}</td>
          <td>
            <ButtonGroup>
              <Button onClick={() => this.adjustAmount(value, 0.5)}> Low </Button>
              <Button onClick={() => this.adjustAmount(value, 1)}> Regular </Button>
              <Button onClick={() => this.adjustAmount(value, 2)}> High </Button>
            </ButtonGroup>
          </td>
        </tr>
      );
    });

    //{console.log('Creating Info Modal for: ' + this.props.drink.drink)}


    const recipeTable = (
      <Table striped bordered condensed hover>
        <thead>
          <tr>
            <th>Ingredient</th>
            <th>Amount</th>
            <th>Customize</th>
          </tr>
        </thead>
      <tbody>
        {recipeTableBody}
      </tbody>
      </Table>
    )

    return(
      <div>
        <Modal show={this.props.show} onHide={this.props.onHide} bsSize="large" aria-labelledby="contained-modal-title-sm">
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-sm" className="capitalize" > {this.props.drink.name} </Modal.Title>
          </Modal.Header>
          <Modal.Body>

            <h5 className="capitalize"> {this.props.drink.name} Recipe </h5>
          
            {recipeTable}

          </Modal.Body>
          <Modal.Footer>
            <Button bsStyle="success" onClick={() => this.makeDrinkCustom(this.state.recipe)}> Make this drink! </Button>
            <Button onClick={this.props.onHide}>Close</Button>
          </Modal.Footer>
        </Modal>

      </div>
    );
  }

}

