import React from 'react';
import ReactDOM from 'react-dom';

import {Grid, Row, Col, Modal, ButtonGroup, Button, Table} from 'react-bootstrap';

import Select from 'react-select';

var config = require('./config');

// Be sure to include styles at some point, probably during your bootstrapping
//import 'node-modules/react-select/dist/react-select.css';

export default class Dispensers extends React.Component {

  constructor(props){
    super(props)

    this.state = {
      initDispensers: [],
      curDispensers: [],
      IngOpt: []
    };

  }

  componentDidMount(){
      this.getDispensers();

      this.getIngredients();
  }


  alertDispensers(){
    console.log(this.state.curDispensers)
  }

  getDispensers(){
    var getDispensersURL = config.baseURL+'/getDispensers';

    var request = {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      method: "GET",
    };

    fetch(getDispensersURL, request)
      .then( (response) => {
        if(response.status >= 400){
          throw new Error('Bad response from server');
        }
        console.log('Response status from getDispenser endpoint: '+ response.status);
        return response.json();
      })
      .then( (data) => {
        console.log('Dispenser Info Obtained!');
        this.setState({initDispensers: data}); //set initDispensers to JSON obtained from back end query
        this.setState({curDispensers: data}); //set curDispensers to JSON obtained from back end query
        console.log(data)
      });
  }

  setDispensers(dispensers){
    //pushes curDispensers to server to update database
    var setDispensersURL = config.baseURL+'/setDispensers';

    var request = {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      method: "POST",
      body: JSON.stringify({dispensers: dispensers})
    };

    fetch(setDispensersURL, request)
      .then( (response) => {
        if(response.status >= 400){
          throw new Error('Bad response from server');
        }
        console.log('Response status from getDispenser endpoint: '+ response.status);
        return response.json();
      })
      .then( (data) => {
        console.log('Dispensers Set!');
      });
  }

  getIngredients(){
    var getIngredientsURL = config.baseURL+'/getIngredients';

    var request = {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
        },
      method: "GET"
      };

    fetch(getIngredientsURL, request)
      .then( (response) => {
        if(response.status >= 400){
          throw new Error('Bad response from server');
        }
        console.log('Response status from getIngredient endpoint: '+ response.status);
        return response.json();
      })
      .then( (data) => {
        console.log('Ingredient Info Obtained!');
        this.setState({ingredients: data})
        console.log(data);


        //create select options
        const options = this.state.ingredients.map((ingredient) => {
          return(
              {value: ingredient.id, label: ingredient.name}
            );
        });

        console.log('Ingredient Options Created');
        console.log(options);

        this.setState({IngOpt: options})
      });
  }

  onSelectChange(val, ingredient){ //val is dispenser.id
    console.log('dispenser id');
    console.log(val);
    console.log('ingredient');
    console.log(ingredient)

    //get list of dispensers
    var tempDispensers = this.state.curDispensers.slice();

    //log ingredient that was chosen
    console.log('tempDispensers at value')
    console.log(tempDispensers[val-1]);

    //if ingredient chosen was empty then place nothing in that Dispenser
    if(ingredient == null){
      tempDispensers[val-1].ingredient_id = 'None';
      tempDispensers[val-1].ingredientName = 'None';
    }else{ //else get new ingredient and set it into tempDispensers
      tempDispensers[val-1].ingredient_id = ingredient.value;
      tempDispensers[val-1].ingredientName = ingredient.label;
    }

    //now update curDispensers
    this.setState({curDispensers : tempDispensers}); 
  }

  render(){

    const dispenserTableBody = this.state.curDispensers.map((dispenser, index) => {
      //console.log(di.ingredient);
      return( 
        <tr key = {dispenser.id} >
          <td>{dispenser.id}</td>
          <td width="50%">

            <Select // value prop expects opject { value: string | number, label: string }
              name="form-field-name"
              value={{value:dispenser.ingredient_id, label: dispenser.ingredientName}}
              options={this.state.IngOpt}
              onChange={this.onSelectChange.bind(this, dispenser.id)}
            />

          </td>
          <td>{dispenser.servoID}</td>
          <td>{dispenser.pumpID}</td>
        </tr>
      );
    });

    const dispenserTable = (
      <Table striped bordered condensed hover>
        <thead>
          <tr>
            <th>Dispenser</th>
            <th>Ingredient</th>
            <th>Servo ID</th>
            <th>Pump ID</th>
          </tr>
        </thead>
      <tbody>
        {dispenserTableBody}
      </tbody>
      </Table>
    )

    return(
      <div>
        <Modal show={this.props.show} onHide={this.props.onHide} bsSize="large" aria-labelledby="contained-modal-title-sm">
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-sm" className="capitalize" > Dispensers </Modal.Title>
          </Modal.Header>
          <Modal.Body>

            {/*<h5 className="capitalize"> {this.props.drink.name} Recipe </h5>*/}
          
            {dispenserTable}

          </Modal.Body>
          <Modal.Footer>
            <Button bsStyle="success" onClick={() => this.setDispensers(this.state.curDispensers)}> Save Dispenser Settings </Button>
            <Button bsStyle="success" onClick={() => this.alertDispensers()}> ALERT </Button>
            <Button onClick={this.props.onHide}>Close</Button>
          </Modal.Footer>
        </Modal>

      </div>
    );
  }

}

