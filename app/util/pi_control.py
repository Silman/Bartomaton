
##TODO:
# add imports

from app import db
from app.models import *
from . import peripheral_util

#TO DO: actually calculate drink ratios to activate dispensers
#TO DO: Ignore certain ingredients that aren't supposed to be loaded?

def makeDrinkRecipe(recipe):
  #recipe is a JSON object containing drinkIngredients
  print("MAKING A RECIPE")

  #ensure all ingredients have dispenser assigned
  for di in recipe: #for each drink ingredient
    print(f'Recipe Ingredient: {di["ingredient"]}')
    databaseIngredient = Ingredient.query.filter_by(name=di['ingredient']).first()
    print(f'Database Ingredient: {databaseIngredient}')

    if(databaseIngredient.dispenser is None):
      print("No dispenser for ingredient: ", databaseIngredient.name)
      print("Breaking...")
      return
    else:
      print("Ingredient: ", databaseIngredient, " on dispenser: ", databaseIngredient.dispenser)
      print("Amount to pour: ", di['amount'])

  #if you reach here, all ingredients have dispensers

  # If you reach here, all ingredients have active dispensers, make the drink
  for di in recipe: # for each drink ingredient
    # Get dispenser
    databaseDispenser = Ingredient.query.filter_by(name=di['ingredient']).first().dispenser

    pumpID = databaseDispenser.pumpID
    servoID = databaseDispenser.servoID
    length = float(di['amount'])  # Dispense for this amount of time

    print(f'Dispensing {length} units of {databaseDispenser.ingredient} on pump pin {pumpID} and servo pin {servoID}')

    peripheral_util.dislayText('Dispensing:', 2)
    peripheral_util.dislayText(databaseDispenser.ingredient, 3)
    peripheral_util.dispenseLiquid(pumpID, servoID, length)

  return True
  
def makeDispensersFromDatabase():
  # get all dispensers
  # dispenserList = Dispenser.query.all()

  # make dispenser object dictionary keyed by ingredient name
  # dispenserDict = {}
  # for dis in dispenserList:
  #   dispenserDict[dis.ingredient.name] = DispenserObject(gpio, dis.servoPin, dis.pumpPin)

  return True