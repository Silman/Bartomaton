from ..config import decoderControlGPIOPin, decoderControlInhibitPin, decoderControlStrobePin, SERVO_DC_OPEN, SERVO_DC_CLOSE

from .PCA9685 import PWM
from .PCF8574AT import LCD
from .HX711 import HX711

import time
import pigpio
#Create pigpio pi object here
pi = pigpio.pi()

#Servo Controller Object
servoDriver = PWM(pi)

#LCD Controller Object
displayDriver = LCD(pi)

#Load Cell Conroller
loadCellDriver = HX711(pi)

def dispenseLiquid(pumpID, servoID, length):

    activatePump(pumpID)

    setServoPosition(servoID, SERVO_DC_OPEN)

    #TO DO: Delay for length or make while loop here that breaks when load cell hits certain value?
    time.sleep(length)

    setServoPosition(servoID, SERVO_DC_CLOSE)

    deactivatePump()

    return True


def activatePump(pumpID):

  #TO DO: Maybe store pump -> binary as dictionary in configuration

  #convert pumpID into binary
  binaryID = bin(pumpID)[2:].zfill(4)

  decoderBit0 = binaryID[-1]
  decoderBit1 = binaryID[-2]
  decoderBit2 = binaryID[-3]
  decoderBit3 = binaryID[-4]

  #select pump with decoder bits
  pi.write(decoderControlGPIOPin[0], int(decoderBit0))
  pi.write(decoderControlGPIOPin[1], int(decoderBit1))
  pi.write(decoderControlGPIOPin[2], int(decoderBit2))
  pi.write(decoderControlGPIOPin[3], int(decoderBit3))

  #de-inhibit pin
  pi.write(decoderControlInhibitPin, int(0))

  #strobe the decoder to activate
  pi.write(decoderControlStrobePin, int(1))

  #todo: add delay here?

  pi.write(decoderControlStrobePin, int(0))

  return

def deactivatePump():

  #TO DO: Store decoderControlInhibitPin dictionary in config

  #inhibit pin
  pi.write(decoderControlInhibitPin, int(1))

  #select final pump
  pi.write(decoderControlGPIOPin[0], int(1))
  pi.write(decoderControlGPIOPin[1], int(1))
  pi.write(decoderControlGPIOPin[2], int(1))
  pi.write(decoderControlGPIOPin[3], int(1))

  return

def setServoPosition(servoID, position):

  #TO DO: convert position into a PWM signal
  #TO DO: convert servoID into I2C address query
  #TO DO: implement I2C call to Servo control board

  servoDriver.set_duty_cycle(servoID, position)
  
  return True

def displayText(text, line):
  displayDriver.put_line(line, text)

  return
