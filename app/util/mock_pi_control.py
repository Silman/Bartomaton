# Mocks functions that require GPIO control

from app import db
from app.models import *
from time import sleep

def makeDrinkRecipe(recipe):
  #rRecipe is a JSON object containing drinkIngredients
  print("MAKING A RECIPE")

  # Ensure all ingredients have dispenser assigned
  for di in recipe: # For each drink ingredient
    print(f'Recipe Ingredient: {di["ingredient"]}')
    databaseIngredient = Ingredient.query.filter_by(name=di['ingredient']).first()
    print(f'Database Ingredient: {databaseIngredient}')

    if(databaseIngredient.dispenser is None):
      print("No dispenser for ingredient: ", databaseIngredient.name)
      print("Breaking...")
      return
    else:
      print("Ingredient: ", databaseIngredient, " on dispenser: ", databaseIngredient.dispenser)
      print("Amount to pour: ", di['amount'])

  # If you reach here, all ingredients have active dispensers, make the drink
  for di in recipe: #for each drink ingredient
    # Get dispenser
    databaseDispenser = Ingredient.query.filter_by(name=di['ingredient']).first().dispenser

    pumpID = databaseDispenser.pumpID
    servoID = databaseDispenser.servoID
    length = float(di['amount'])  # Dispense for this amount of time

    print(f'Dispensing {length} units of {databaseDispenser.ingredient} on pump pin {pumpID} and servo pin {servoID}')

    # Mock calls to Gpio control go here
    sleep(length)

  return True
