import os.path
import imp

from migrate.versioning import api
import numpy as np

from app.config import SQLALCHEMY_DATABASE_URI
from app.config import SQLALCHEMY_MIGRATE_REPO
from app import db
from app.models import *

print('Working on Database: ', db)

def createDatabase():

  #create database file
  db.create_all()

  #create database repository
  if not os.path.exists(SQLALCHEMY_MIGRATE_REPO):
      api.create(SQLALCHEMY_MIGRATE_REPO, 'database repository')
      api.version_control(SQLALCHEMY_DATABASE_URI, SQLALCHEMY_MIGRATE_REPO)
  else:
      api.version_control(SQLALCHEMY_DATABASE_URI, SQLALCHEMY_MIGRATE_REPO,
                          api.version(SQLALCHEMY_MIGRATE_REPO))

def migrateDatabase():
  #get database version
  v = api.db_version(SQLALCHEMY_DATABASE_URI, SQLALCHEMY_MIGRATE_REPO)

  #creat migration script to go to next version
  migration = SQLALCHEMY_MIGRATE_REPO + ('/versions/%03d_migration.py' % (v+1))

  #create temporary model
  tmp_module = imp.new_module('old_model')

  #store old model
  old_model = api.create_model(SQLALCHEMY_DATABASE_URI, SQLALCHEMY_MIGRATE_REPO)

  #execute
  exec(old_model, tmp_module.__dict__)
  
  #make update script to go to new model
  script = api.make_update_script_for_model(SQLALCHEMY_DATABASE_URI,
                                            SQLALCHEMY_MIGRATE_REPO,
                                            tmp_module.meta, db.metadata)

  #open the migration script and write the script
  open(migration, "wt").write(script)

  #upgrade the database
  api.upgrade(SQLALCHEMY_DATABASE_URI, SQLALCHEMY_MIGRATE_REPO)

  #get new database version
  v = api.db_version(SQLALCHEMY_DATABASE_URI, SQLALCHEMY_MIGRATE_REPO)

  #print new database info
  print('New migration saved as ' + migration)
  print('Current database version: ' + str(v))

def upgradeDatabase():

  #upgrade database
  api.upgrade(SQLALCHEMY_DATABASE_URI, SQLALCHEMY_MIGRATE_REPO)

  #get upgraded database version
  v = api.db_version(SQLALCHEMY_DATABASE_URI, SQLALCHEMY_MIGRATE_REPO)

  #print database info
  print('Current database version: ' + str(v))

def downgradeDatabase():

  #get current database version
  v = api.db_version(SQLALCHEMY_DATABASE_URI, SQLALCHEMY_MIGRATE_REPO)

  #downgrade database
  api.downgrade(SQLALCHEMY_DATABASE_URI, SQLALCHEMY_MIGRATE_REPO, v - 1)

  #update database version
  v = api.db_version(SQLALCHEMY_DATABASE_URI, SQLALCHEMY_MIGRATE_REPO)

  #print database info
  print('Current database version: ' + str(v))


def makeDrinkDbFromCSV():
  import pandas as pd

  drinkDataFrameRaw = pd.read_csv('./app/database/drinks.csv')
  drinkDataFrame = drinkDataFrameRaw.where((pd.notnull(drinkDataFrameRaw)), None)

  #get ingredients
  headers = list(drinkDataFrame)

  ingredientsList = headers[1:]

  #print(ingredientsList)

  for index, row in drinkDataFrame.iterrows(): #for each drink
    #print(row)
    drinkName = str(row[0])

    dbDrink = Drink(drinkName)

    for ingredient in ingredientsList:
      if row[ingredient] is not None:
        #print(str(ingredient))
        #print(float(row[ingredient]))

        dbDrink.recipe.append(DrinkIngredient(Ingredient.get_or_create(str(ingredient)), float(row[ingredient])))

    print(dbDrink)
    print(dbDrink.recipe)

    db.session.add(dbDrink)
    db.session.commit()

    print('\n')

def generateDispensers(num):

  #TODO: Add call in run.py sript

  for i in range(num):
    dis = Dispenser(i,i)
    db.session.add(dis)
    db.session.commit()

  return

