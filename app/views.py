from app import app, db
import os

from flask import Flask, request, render_template, jsonify, send_from_directory

from app.models import *
import time
import json

from app.config import staticdir, IS_DEVICE

# Either load real device library for GPIO calls or mock
if(IS_DEVICE):
  print("Running on Pi, loading Pi control module.")
  import app.util.pi_control as pi_control
else:
  print("Running on Host, loading Mock control module.")
  import app.util.mock_pi_control as pi_control

@app.route("/")
def hello():
    #print(os.path.join(staticdir, 'html','index.html'))

    #Create Dispenser objects
    print("WOWOWO)")
    return send_from_directory(os.path.join(staticdir, 'html'),'index.html')

@app.route("/drink", methods=['GET'])
def load_drinks():

    drinks = Drink.query.all()

    return (json.dumps([d.to_dict() for d in drinks]))

@app.route("/makeDrinkID", methods=['POST'])
def makeDrinkID():

  #Get POST request json data
  jsonData = request.get_json()
  #get drink ID from jsonData in request
  drinkID = jsonData['drink_id']
  #get drink from database using id
  drink = Drink.query.get(drinkID)
  print("Making Drink: ", drink.name)
  #get drink recipe
  recipe = [ di.to_dict() for di in drink.recipe]
  #call drink making utility function
  pi_control.makeDrinkRecipe(recipe) 

  return(request.data)

@app.route("/makeDrinkCustom", methods=['POST'])
def makeDrinkCustom():

  #Get POST request json data
  jsonData = request.get_json()
  #get recipe from jsonData
  recipe = jsonData['recipe']

  print(recipe)

  pi_control.makeDrinkRecipe(recipe)

  return (request.data)


@app.route("/setDispensers", methods=['POST'])
def setDispensers():

  #print(request.data)

  jsonData = request.get_json()

  print(jsonData)

  #Clear dispensers ingredients from database
  for dispenser in jsonData['dispensers']:
    d = Dispenser.query.filter_by(id=dispenser['id']).first()
    d.ingredient = None
    db.session.add(d)
    db.session.commit()

  #write new ingredients to dispensers
  for dispenser in jsonData['dispensers']:
    d = Dispenser.query.filter_by(id=dispenser['id']).first()
    i = Ingredient.query.filter_by(id=dispenser['ingredient_id']).first()
    d.ingredient = i
    print(d)

    db.session.add(d)
    db.session.commit()

  return (request.data)

@app.route("/getDispensers", methods=['GET'])
def getDispensers():

  dispensers = Dispenser.query.all()
  print(dispensers)

  return (json.dumps([d.to_dict() for d in dispensers]))

@app.route("/getIngredients", methods=['GET'])
def getIngredients():

  ingredients = Ingredient.query.all()
  print(ingredients)

  return (json.dumps([i.to_dict() for i in ingredients]))  

# @app.route('/favicon.ico')
# def favicon():
#     return send_from_directory(os.path.join(app.root_path, 'static/img'),
#                                'favicon.ico', mimetype='image/vnd.microsoft.icon')