from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS

import app.config
from app.config import *

# create app instance
app = Flask(__name__, static_url_path='/static')
CORS(app)
# load config
app.config.from_object(config)

# create database
db = SQLAlchemy(app)

#initlization  code
def setup():
  print("initializing...")
  ##remake all dispensers from new database
  #dispenserDict = makeDrink.makeDispensersFromDatabase()
  #print("Dispensers Initalized")

#run setup
setup()

# import modules -- MUST BE DONE LAST OTHERWISE CIRCULAR IMPORT OCCURS
from app import views, models
